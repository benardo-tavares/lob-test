import React, { useState } from "react";
import classNames from "classnames";
import AutoComplete from "../AutoComplete";
import PropTypes from "prop-types";

const suggestionParser = (response) => response.data;

const SuggestionCard = ({ suggestion }) => (
  <div className="suggestion-card">
    <p className="card-name">{suggestion.name}</p>
    <address>
      {suggestion.address_line1} <br />
      {suggestion.address_city}, {suggestion.address_state},{" "}
      {suggestion.address_zip} <br />
      {suggestion.address_country}
    </address>
  </div>
);

const SuggestionItem = (props) => {
  const { suggestion, active } = props;
  return (
    <div
      {...props}
      className={classNames({
        "suggestion-item": true,
        "suggestion-item__active": active,
      })}
    >
      <SuggestionCard suggestion={suggestion} />
      <div className="suggestion-item-id">
        <p>{suggestion.id}</p>
      </div>
    </div>
  );
};

SuggestionItem.propTypes = {
  suggestion: PropTypes.isRequired,
};

SuggestionItem.propTypes = {};

const AddressAutoComplete = () => {
  const [value, setValue] = useState("");
  const [selection, setSelection] = useState(null);

  const fetchUrl = ({ searchQuery }) => {
    const API_END_POINT = "https://api.lob.com/v1/search";
    const params = {
      q: searchQuery,
      types: "addresses",
      offset: 0,
      limit: 3,
    };
    return `${API_END_POINT}?q=${params.q}&types=${params.types}&offset=${params.offset}&limit=${params.limit}`;
  };

  const headers = {
    Authorization: `Basic dGVzdF84ZGRhYWQzNWRjMDIyNjBhZThhNGU2ZTMzZDlmM2FkZTdhZTo=`,
  };

  if (selection) {
    return (
      <div className="suggestion-card__wrapper">
        <SuggestionCard suggestion={selection} />
        <div
          className="suggestion-card__close"
          onClick={() => setSelection(null)}
        >
          X
        </div>
      </div>
    );
  }

  return (
    <AutoComplete
      value={value}
      headers={headers}
      onChange={setValue}
      onSelect={setSelection}
      fetchUrl={fetchUrl}
      suggestionParser={suggestionParser}
    >
      {({ inputProps, getSuggestionProps, suggestions, error, loading }) => {
        if (error) return <div>We have an error..</div>;
        return (
          <div>
            <input {...inputProps({ placeholder: "Search for something.." })} />
            {loading && <div className="loading-status">Searching ...</div>}
            {!loading && value.length !== 0 && suggestions.length === 0 && (
              <div className="loading-status">No Results</div>
            )}
            {suggestions.length > 0 && (
              <div>
                {suggestions.map((suggestion) => (
                  <SuggestionItem
                    {...getSuggestionProps(suggestion)}
                    suggestion={suggestion}
                  ></SuggestionItem>
                ))}
              </div>
            )}
          </div>
        );
      }}
    </AutoComplete>
  );
};

export default AddressAutoComplete;
