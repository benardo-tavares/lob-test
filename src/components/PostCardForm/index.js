import React from "react";

import AddressAutoComplete from "../AddressAutoComplete";

const PostCardForm = () => (
  <form>
    <div className="form-group">
      <label>Description:</label>
      <input type="text" />
    </div>
    <div className="form-group">
      <label>To:</label>
      <AddressAutoComplete />
    </div>
    <div className="form-group">
      <label>From:</label>
      <input type="text" />
    </div>
    <div className="form-group">
      <label>Front:</label>
      <input
        type="text"
        required
        defaultValue="https://s3-us-west-2.amazonaws.com/lob-assets/4x6_pc_front_ex.pdf"
      />
    </div>
    <div className="form-group">
      <label>Back:</label>
      <input
        type="text"
        required
        defaultValue="https://s3-us-west-2.amazonaws.com/lob-assets/4x6_pc_back_ex.pdf"
      />
    </div>
    <input className="btn" type="submit" value="Submit" />
  </form>
);

export default PostCardForm;
