import React from "react";
import PostCardForm from "./components/PostCardForm";
import "./App.css";

function App() {
  return (
    <div className="App">
      <PostCardForm />
    </div>
  );
}

export default App;
